# KotlinLab: Easy and effective MATLAB-like scientific programming with Kotlin and Java JShell 



## Installation

The installation of KotlinLab is very simple: 

  * *Step 1* Download and unzip the .zip download.
  
  * *Step 2* Run the proper script for your platform, i.e. the .sh scripts for Unix like systems and the .bat scripts for Windows. 

## Building 

KotlinLab can be built with the superb JetBrains IntelliJ IDEA environment.

One important point however for successfully building the project,
is that from the "Settings" -> "Build, Execution, Deployment" -> "Annotation Processors",
we should disable the option "Enable annotation processing".


## Project Summary

The KotlinLab environment aims to provide a MATLAB/Scilab like scientific computing platform that is supported by a scripting engine of the Kotlin language. The KotlinLab user can work either with a MATLAB-like command console, or with a flexible editor based on the rsyntaxarea  component, that offers more convenient code development. KotlinLab supports extensive plotting facilities and can exploit effectively a lot of powerful Java scientific libraries, as JLAPACK , Apache Common Maths , EJML , MTJ , NUMAL translation to Java , Numerical Recipes Java translation , Colt etc. 

KotlinLab aims to explore the superb DSL construction facilities of the Kotlin Language and the JShell environment of Java to provide an effective scientific programming environment for the JVM.


## Basic Keystrokes of KotlinLab

To execute the current line or a piece of selected Kotlin code in KotlinLab use the F6 key.

To clear the output use the F5 key.

To execute the current line or a piece of selected Java code in KotlinLab with the JShell, use the F7 key.


## KotlinLab Developer

Stergios Papadimitriou

International Hellenic University

Dept of Informatics 

Kavala, Greece

email: sterg@cs.ihu.gr
