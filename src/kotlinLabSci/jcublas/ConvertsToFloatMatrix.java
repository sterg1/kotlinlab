package kotlinLabSci.jcublas;

public interface ConvertsToFloatMatrix {
    public FloatMatrix convertToFloatMatrix();
}
